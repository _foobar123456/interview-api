<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use DB;

class AuthenticateController extends Controller
{

    public function authenticate(Request $request)
    {
    	$username = $request->input('username');
    	$password = $request->input('password');

        $user = User::where( compact('username','password') )->first();

        $authToken = '';
        if( !empty( $user ) )
        {
            $authenticated = true;
            $authToken = md5( $username . $password . time() );
            User::where( compact('username','password') )->update( ['auth_token' => $authToken]);
        } else {
            $authenticated = false;
        }

    	$data = [
    		'status' => ( $authenticated ) ? 'ok' : 'invalid credentials',
    		'authToken' => $authToken
    	];
    	return response()->json($data, ( $authenticated ) ? 200 : 401, []);
    }

    private function lookupUser( $username, $password )
    {
    	$result = DB::table('users')->where( compact( 'username', 'password') )->get();
    	if( empty( $result ) ) throw new Exception('User not found');
    	return $result;
    }

}
