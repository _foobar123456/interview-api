<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class User extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password', 'auth_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'auth_token'
    ];

    public static function isTokenInUse( $authToken )
    {
        $result = User::where( ['auth_token' => $authToken] )->get();
        return ( $result->isEmpty() ) ? false : true;
    }

}
