<?php

use Illuminate\Database\Seeder;

class add_foo_user_to_users_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert([
            'username' => 'foo',
            'password' => 'bar',
            'auth_token' => ''
        ]);
    }
}
