<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Events;

class EventController extends Controller
{

    public function index(Request $request)
    {
    	$events = Events::all();
    	return $events;
    }

    // public function read( $id )
    // {
    // 	$event = Events::find( ['id' => $id] )->attendees();
    // 	return $event;
    // }

}
