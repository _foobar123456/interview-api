<?php

use Illuminate\Database\Seeder;

class add_events_to_events_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('events')->insert([
            'id' => 1,
            'organization' => 'U-electronics',
            'logo' => 'http://lorempixel.com/50/50/',
            'title' => 'GenEl Conf 2016',
            'description' => 'GenEl is soliciting technical papers for oral and poster presentations at their 35th annual conference on the topic of VR. Mark Zuckerberg stated that: “VR is the next platform..."',
            'start_time' => '25/11/2016 09:00',
            'end_time' => '25/11/2016 14:00',
            'location' => 'Las Vegas'
        ]);
        DB::table('events')->insert([
            'id' => 2,
            'organization' => 'Zoomtechi',
            'logo' => 'http://lorempixel.com/50/50/',
            'title' => '15th Bio Conference 2016',
            'description' => '15th Bio Conference 2016 "The ethics of wearing shoes". Ten scholars of philosophy will be accepted to participate in this round-table seminar',
            'start_time' => '04/07/2016 10:00',
            'end_time' => '05/07/2016 17:00',
            'location' => 'San Francisco'
        ]);
        DB::table('events')->insert([
            'id' => 3,
            'organization' => 'Silverbam',
            'logo' => 'http://lorempixel.com/50/50/',
            'title' => 'Yet another framework - SilverbamJS',
            'description' => 'YAF SilverbamJS is the only event focused on the language of the interconnected ecosystem of technology solutions, services, apps and platforms that are already powering the workplace and personal lives.',
            'start_time' => '13/09/2016 11:30',
            'end_time' => '13/09/2016 12:30',
            'location' => 'Vancouver'
        ]);
	}
}
