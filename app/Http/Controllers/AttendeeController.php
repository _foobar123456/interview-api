<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Attendees;


class AttendeeController extends Controller
{
	public function read( $id )
	{
		$attendees = Attendees::where( 'event_id', $id )->get();
		return $attendees;
	}
}
