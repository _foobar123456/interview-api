<?php

use Illuminate\Database\Seeder;

class add_attendees_to_attendees_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('attendees')->insert([
            'id' => 1,
            'event_id' => 1,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "John Doe",
            'title' => "Executive Sales Manager",
            'profile' => "I lead the sales team towards success!"
        ]);
        DB::table('attendees')->insert([
            'id' => 2,
            'event_id' => 1,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "Jane Doe",
            'title' => "Vice President",
            'profile' => "Built the userbase for our startup from 10 to 1 million active users. I am passionate about technology with a capital P."
        ]);
        DB::table('attendees')->insert([
            'id' => 3,
            'event_id' => 1,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "Eric Johnson",
            'title' => "Web Developer",
            'profile' => "I love Angular!"
        ]);
		DB::table('attendees')->insert([
            'id' => 4,
            'event_id' => 1,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "John Doe",
            'title' => "Executive Sales Manager",
            'profile' => "I lead the sales team towards success!"
        ]);
        DB::table('attendees')->insert([
            'id' => 5,
            'event_id' => 1,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "Jane Doe",
            'title' => "Vice President",
            'profile' => "Built the userbase for our startup from 10 to 1 million active users. I am passionate about technology with a capital P."
        ]);
        DB::table('attendees')->insert([
            'id' => 6,
            'event_id' => 1,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "Bérénice Sagnier",
            'title' => "Web Developer",
            'profile' => "I love Angular!"
        ]);

        //

		DB::table('attendees')->insert([
            'id' => 7,
            'event_id' => 2,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "Barack Obama",
            'title' => "President of the United States of America",
            'profile' => "Leader of the free world"
        ]);
        DB::table('attendees')->insert([
            'id' => 8,
            'event_id' => 2,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "Jim Carrey",
            'title' => "Actor",
            'profile' => "One thing I hope I'll never be is drunk with my own power. And anybody who says I am will never work in this town again."
        ]);
        DB::table('attendees')->insert([
            'id' => 9,
            'event_id' => 2,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "Mark Zuckerberg",
            'title' => "Facebook",
            'profile' => "One of the world's youngest billionaires."
        ]);

        //
        
		DB::table('attendees')->insert([
            'id' => 10,
            'event_id' => 3,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "Tom Cruise",
            'title' => "IMF Agent",
            'profile' => "Dun dun duuu da - dun dun duuu duuuh TADA!"
        ]);
        DB::table('attendees')->insert([
            'id' => 11,
            'event_id' => 3,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "Betty Blake",
            'title' => "Senior Marketing Assistent",
            'profile' => "Managing day-to-day operations for marketing, web-site and general correspondence with customers."
        ]);
        DB::table('attendees')->insert([
            'id' => 12,
            'event_id' => 3,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "Mosiah Garvey",
            'title' => "Human Resources",
            'profile' => "Head hunting and general operations."
        ]);
		DB::table('attendees')->insert([
            'id' => 13,
            'event_id' => 3,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "Michael Xing",
            'title' => "ËVice President",
            'profile' => "Extensive experience in sales and marketing leadership with global hotel brands"
        ]);
        DB::table('attendees')->insert([
            'id' => 14,
            'event_id' => 3,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "Brian Williamson",
            'title' => "DevOps",
            'profile' => "AWS, Digital Ocean, Rackspace, Azure, Chef, Ansible, New Relic, Ruby, Python, Fish, Bash, Jenkins, Travis-CI, CodeShip, MongoDB, Redis, Postgres, Maria DB, "
        ]);
        DB::table('attendees')->insert([
            'id' => 15,
            'event_id' => 3,
            'avatar' => "http://lorempixel.com/50/50/",
            'name' => "Jim Jones",
            'title' => "Web Developer",
            'profile' => "."
        ]);

	}
}
