<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use App\User;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $authToken = $request->header('auth_token');
        $authenticated = User::isTokenInUse($authToken);
        if( !$authenticated ) return response('Unauthorized.', 401);

        return $next($request);
    }
}
