<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(add_foo_user_to_users_table::class);
        $this->call(add_events_to_events_table::class);
        $this->call(add_attendees_to_attendees_table::class);
    }
}
